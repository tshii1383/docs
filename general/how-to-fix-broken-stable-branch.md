### How to fix a broken stable branch?

GitLab releases depend on stable branches, as such, it's important to ensure these branches have green pipelines. If a failure in a stable branch is found:

1. A merge request targeting the stable branch must be opened with the fix on the Canonical repository.
1. The merge request should be reviewed and approved, according to our [approval guidelines].
1. Once the merge request is approved, it should be assigned to the [Release Managers]. Optionally post a message on the `#releases` Slack channel.
1. Release Managers will merge the merge request.

Stable branches are protected branches and therefore changes merged into the canonical repository will be automatically propagated to the security and dev repositories.

[approval guidelines]: https://docs.gitlab.com/ee/development/code_review.html#approval-guidelines
[Release Managers]: https://about.gitlab.com/community/release-managers
