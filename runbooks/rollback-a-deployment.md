# Overview

This document describes the procedure to rollback a GitLab.com deployment by
re-deploying a previous package. Rollbacks can help recover from bug-related
incidents.

[[_TOC_]]

# Before starting - check for migrations

A rollback should not be attempted with a deployment containing post-deployment
migrations as they can leave the database in an unknown state. Determining if
it's safe to rollback is currently automate by a chatops command, but a [manual
procedure](#manually-checking-for-migrations) is still described below in this
document.

To determine if is safe to rollback run the following chatops command:

```
/chatops run rollback check gstg
```

The last parameter is the environment, to check production use the following command:

```
/chatops run rollback check gprd
```

![chatops command](rollback-a-deployment/chatops-run-rollback-check.png)

The output shows the number and type of migrations included in the last package
as well as a link to the current and previous deployed commits and a comparison link.

# Rolling back

## 1. Find the package to roll back to

The package name we want to roll back to is displayed in the output from the
`rollback check` command above. Copy it to pass to the `deploy` command in the
next step.

If we're unable to determine the package name automatically, expand the next
section for manual instructions.

<details>
  <summary>Manual steps</summary>

Replace `SHA` with the SHA of the deployment to roll back to (985b57c4ca9 in the
above example) and open the following link in your browser:

```
https://gitlab.com/gitlab-org/security/omnibus-gitlab/-/tags?utf8=%E2%9C%93&search=SHA
```

You should see a single tag with a name like so:

```
13.9.202102091820+985b57c4ca9.7ad6df8e35c
```

It's possible for more than one tag to be displayed. In that case, you should
pick the most recent tag based on the tagging date (`202102091820` in the above
example).

To obtain the package version, simply replace the `+` with a `-`, like so:

```
13.9.202102091820-985b57c4ca9.7ad6df8e35c
```
</details>

## 2. Perform the rollback

You can now initiate a rollback using chatops. To roll back staging:

```
/chatops run deploy 13.9.202102091820-985b57c4ca9.7ad6df8e35c --rollback
```

And to roll back production:

```
/chatops run deploy 13.9.202102091820-985b57c4ca9.7ad6df8e35c --rollback --production
```

An alternative [manual procedure](#manual-rollback) is described below in this
document.

## OPTIONAL - gitaly and preafect

By default the rollback pipeline **will not downgrade gitaly and preafect**.

If a rollback is desired, a final stage with manual jobs is provided as part of the
rollback pipeline. This optional stage can run as soon as the prepare stage is completed.

# After a rollback

After a rollback has been completed, it's important to have the problem that
lead to a rollback fixed. The exact process of this may differ based on the
reason for the rollback, but in most cases this will involve asking the
appropriate developers to open a merge request that addresses the root cause.

**The rollback is now complete**

# OPTIONAL - In case of chatops failure

The rollback is completed, here follow a list of manual action to run a rollback in case of inability
to run chatops commands.

## Manually checking for migrations

### 1. Pick the environment

First, open the environment page of the environment you wish to roll back:

* [staging](https://gitlab.com/gitlab-org/security/gitlab/-/environments/1516167)
* [canary](https://gitlab.com/gitlab-org/security/gitlab/-/environments/1913972)
* [production](https://gitlab.com/gitlab-org/security/gitlab/-/environments/1700351)

### 2. Determine the deploy to roll back to

Next, determine what deploy you want to roll back. In almost (if not all) cases
this will simply be the latest deploy recorded. With that deploy noted down,
pick the deploy that came before it. This will be the deploy to roll back to.

Example: the latest deploy is deploy #1991, which deployed commit 9c71509c. The
previous deploy is #1986 which deployed commit 8ce1730f.

### 3. Compare the changes to see if there are migrations

Open the following link in your browser:

```
https://gitlab.com/gitlab-org/security/gitlab/-/compare/PREVIOUS...LATEST
```

Replace `PREVIOUS` with the SHA of the deploy to roll back to (8ce1730f in the
above example), and replace `LATEST` with the SHA of the current deployment
(9c71509c).

With the link open, look for any newly added files in the `db/post_migrate`
directory. If no new files are added, it's safe to roll back.

## Manual rollback

How to start a manual rollback pipeline is described in
[Creating a new deployment for _rolling back_ GitLab](../general/deploy/gitlab-com-deployer.md#creating-a-new-deployment-for-rolling-back-gitlab)

